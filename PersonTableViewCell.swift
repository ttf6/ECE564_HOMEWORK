//
//  PersonTableViewCell.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/20/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    
    //MARK: Properties
    var personView: UIViewController?
    var navigation: UINavigationController?
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var detailsButton: UIButton!


    @IBAction func detailsButton(_ sender: UIButton) {
        if(personView != nil){
            navigation?.pushViewController(personView!, animated: true)
        }
    }

    
    @IBOutlet weak var personImage: UIImageView!
    
    @IBOutlet weak var descriptionText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
