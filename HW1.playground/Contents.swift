import UIKit

/*:
 ### ECE 564 HW1 assignment
 In this first assignment, you are going to create a base data model for storing information about the students, TAs and professors in ECE 564. You need to populate your data model with at least 3 records, but can add more.  You will also provide a search function ("whoIs") to search an array of those objects by first name and last name.
 I suggest you create a new class called `DukePerson` and have it subclass `Person`.  You also need to abide by 2 protocols:
 1. BlueDevil
 2. CustomStringConvertible
 
 I also suggest you try to follow good OO practices by containing any properties and methods that deal with `DukePerson` within the class definition.
 */
/*:
 In addition to the properties required by `Person`, `CustomStringConvertible` and `BlueDevil`, you need to include the following information about each person:
 * Their degree, if applicable
 * Up to 3 of their best programming languages as an array of `String`s (like `hobbies` that is in `BlueDevil`)
 */
/*:
 I suggest you create an array of `DukePerson` objects, and you **must** have at least 3 entries in your array for me to test:
 1. Yourself
 2. Me (my info is in the slide deck)
 3. One of the TAs (also in slide deck)
 */
/*:
 Your program must contain the following:
 - You must include 4 of the following - array, dictionary, set, class, function, closure expression, enum, struct: I used a set, array, function, class, enum
 - You must include 4 different types, such as string, character, int, double, bool, float: I used string, int, bool,
 - You must include 4 different control flows, such as for/in, while, repeat/while, if/else, switch/case: I did for/in, if/else, break, switch
 - You must include 4 different operators from any of the various categories of assignment, arithmatic, comparison, nil coalescing, range, logical: I did arithmatic, assignment, nil coalescing, logical and comparison operations
 */
/*: 
 Base grade is a 45 but more points can be earned by adding additional functions besides whoIs (like additional search), extensive error checking, concise code, excellent OO practices, and/or excellent, unique algorithms
 */
/*:
 Below is an example of what the string output from `whoIs' should look like:
 
     Ric Telford is from Morrisville, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Biking, Hiking and Golf.
 */

enum Gender {
    case Male
    case Female
}

enum GenderPN : String {
    case He = "He"
    case She = "She"
}

class Person {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
}

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}

protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}
//: ### START OF HOMEWORK 
//: Do not change anything above.
//: Put your code here:

class DukePerson: Person, CustomStringConvertible, BlueDevil{
    
    private let languageLimit = 3
    private let hobbyLimit = 3.0
    var hobbies: [String]
    private var languages: [String]
    var role: DukeRole
    private var major = Set<String>()
    private var isMe: Bool
    private var degree: String
    
    init(firstName:String, lastName:String){
        self.hobbies = [String]()
        self.languages = [String]()
        self.major = Set<String>()
        self.role = DukeRole.Student
        self.degree = "none"
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        super.init()
        self.firstName = firstName
        self.lastName = lastName
    }
    
    init(firstName:String, lastName:String, hobbies:[String], languages:[String],role:DukeRole, whereFrom: String, major:Set<String>, degree:String){
        self.hobbies = hobbies
        self.languages = languages
        self.role = role
        self.major = major
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        self.degree=degree
        super.init()
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
    }
    
    func addHobby(hobby: String){
        if(!self.hobbies.contains(hobby)){
           self.hobbies.append(hobby)
        }
    }
    
    func addLanguage(language: String){
        if(!self.languages.contains(language)){
           self.languages.append(language)
        }
    }
    
    func addMajor(major: String){
        self.major.insert(major)
    }
    
    func setHobbies(hobbies: [String]){
        self.hobbies = hobbies
    }
    
    func setLanguages(languages: [String]){
        self.languages = languages
    }
    
    func setMajor(major: Set<String>){
        self.major = major
    }
    
    func setDegree(degree: String){
        self.degree = degree
    }
    
    func setRole(role: DukeRole){
        self.role = role
    }
    
    func setWhereFrom(whereFrom: String){
        self.whereFrom = whereFrom
    }
    
    func getFirstName()->String{
        return self.firstName
    }
    
    func getLastName()->String{
        return self.lastName
    }
    
    func hasAttribute(index: String)->Bool{
        if(searchListUtility(index: index, list: self.languages)){
            return true
        }
        if(searchListUtility(index: index, list: self.hobbies)){
            return true
        }
        if(self.major.contains(index)){
            return true
        }
        if(index == self.role.rawValue){
            return true
        }
        if(index == self.whereFrom){
            return true
        }
        if(index == self.lastName){
            return true
        }
        if(index == self.firstName){
            return true
        }
        if(index == "\(self.firstName) \(self.lastName)"){
            return true
        }
        return false
    }
    
    private func searchListUtility(index:String, list:[String])->Bool{
        for element in list{
            if(element == index){
                return true
            }
        }
        return false
    }
    
    private func descriptionLanguages(baseResponse: String, genderPronoun: String)->String{
        var response = baseResponse
        if(self.languages.count > 0){
            let insert = self.isMe ? "I am " : "\(genderPronoun) is "
            response = "\(response) \(insert)proficient in "
            response = descriptionListUtility(limit:self.languageLimit, message: response, list:self.languages)
        }
        return response
    }
    
    private func descriptionHobbies(baseResponse: String)->String{
        var response = baseResponse
        if(self.hobbies.count>0){
            let insert = self.isMe ? "I enjoy " : "\(self.firstName) enjoys "
            response = "\(response)When not in class, \(insert)"
            response = descriptionListUtility(limit: Int(self.hobbyLimit), message: response, list: self.hobbies)
        }
        return response
    }
    
    private func descriptionListUtility(limit: Int, message: String, list:[String])->String{
        var response = message
        for (index,element) in list.enumerated(){
            if(index == 0){
                response = "\(response)\(element)"
            }
            else if(index == limit - 1 && limit != 0){
                response = "\(response) and \(element). "
                break
            }
            else if(index < list.count - 1){
                response = "\(response), \(element)"
            }
            else{
                response = "\(response) and \(element). "
            }
        }
        return response
    }
    
    var description: String{
        let genderPronoun = self.gender == Gender.Male ? GenderPN.He.rawValue : GenderPN.She.rawValue
        let insertIntro = self.isMe ? "My name is \(self.firstName) \(self.lastName) and I am " : "\(self.firstName) \(self.lastName) is "
        //let insertEnd = self.isMe ? "am" : "is"
        var insertEnd = "am"
        switch self.isMe{
        case true: insertEnd = "am"
        case false: insertEnd = "is"
        }
        let major = self.major.first
        let end = self.role == DukeRole.Student && major != nil ? "\(self.role) studying \(major ?? "major")." : "\(self.role)."
        let baseResponse = "\(insertIntro)from \(self.whereFrom) and \(insertEnd) a \(end)"
        let languageResponse = descriptionLanguages(baseResponse: baseResponse, genderPronoun: genderPronoun)
        let finalResponse = descriptionHobbies(baseResponse: languageResponse)
        return finalResponse
    }
    
    
}

//: ### END OF HOMEWORK
//: Uncomment the line below to test your homework.
//: The "whoIs" function should be defined as `func whoIs(_ name: String) -> String {   }`
var Teddy = DukePerson(firstName: "Teddy", lastName: "Franceschi")
Teddy.setWhereFrom(whereFrom: "Las Vegas, Nevada")
Teddy.addLanguage(language: "Java")
Teddy.addLanguage(language: "Python")
Teddy.addLanguage(language: "TypeScript");
Teddy.addLanguage(language: "C")
Teddy.addLanguage(language: "C++")
Teddy.addHobby(hobby: "Hiking")
Teddy.addHobby(hobby: "Powerlifting")
Teddy.addHobby(hobby: "Running")
Teddy.addMajor(major: "ECE")
Teddy.addMajor(major: "CS")

var Robert = DukePerson(firstName: "Robert", lastName: "Steilberg")
Robert.setWhereFrom(whereFrom: "Richmond, Virginia")
Robert.addLanguage(language: "Javascript")
Robert.addLanguage(language: "Ruby")
Robert.addLanguage(language: "TypeScript");
Robert.addHobby(hobby: "Smash Bros")
Robert.addHobby(hobby: "Complaining")
Robert.addHobby(hobby: "Driving me crazy")
Robert.addMajor(major: "CS")

var Professor = DukePerson(firstName: "Ric", lastName: "Telford")
Professor.setWhereFrom(whereFrom: "Morrisville, NC")
Professor.setLanguages(languages:["swift","C","C++"])
Professor.setHobbies(hobbies: ["Biking","Hiking","Golf"])
Professor.setRole(role: DukeRole.Professor)

var TA = DukePerson(firstName: "Gilbert", lastName: "Brooks")
TA.setWhereFrom(whereFrom: "Shelby,NC")
TA.setDegree(degree:"BA")
TA.addMajor(major:"CS")
TA.addMajor(major:"AAS")
TA.setHobbies(hobbies: ["Computer Vision projects","tennis","traveling"])
TA.setLanguages(languages: ["Swift","Python","Java"])
TA.setRole(role: DukeRole.TA)

func queryIndex(index:String)->[DukePerson]{
    var list = [DukePerson]()
    for element in dataBase {
        if(element.hasAttribute(index: index)){
            list.append(element)
        }
    }
    return list
}

func whoIs(input: String)->String{
    return (queryIndex(index:input).count >= 0) ? queryIndex(index:input)[0].description : "not found"
}

var dataBase = [DukePerson]()
dataBase.append(Teddy)
dataBase.append(Robert)
dataBase.append(Professor)
dataBase.append(TA)

for dp in dataBase{
    print(whoIs(input: "\(dp.getFirstName()) \(dp.getLastName())"))
    print("\n")
}


//print(whoIs(input:"Ric Telford"))
print(Robert.hasAttribute(index: "Male"))




