//
//  CoolButton.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation
import UIKit

class CoolButton: UIButton{
    required init(frame: CGRect, title:String) {
        super.init(frame:frame)
        backgroundColor = UIColor.blue
        setTitleColor(UIColor.white, for: .normal)
        setTitle(title, for: .normal)
        layer.cornerRadius = frame.height/2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func disable(){
        self.isUserInteractionEnabled = false
    }
    
    func reEnable(){
        self.isUserInteractionEnabled = true
    }
    
}
