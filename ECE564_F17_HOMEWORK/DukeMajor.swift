//
//  DukeMajor.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation

enum DukeMajor : String{
    case BA = "BA"
    case BS = "BS"
    case MS = "MS"
    case MEng = "MEng"
    case MBA = "MBA"
    case PhD = "PhD"
    case MD = "MD"
}
