//
//  DukeRole.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
}
