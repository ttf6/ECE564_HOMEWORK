//
//  Gender.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation

enum Gender: String {
    case Male = "Male"
    case Female = "Female"
}
