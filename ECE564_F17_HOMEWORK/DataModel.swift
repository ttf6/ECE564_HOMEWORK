//
//  DataModel.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Ric Telford on 7/16/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit
import os.log

/*:
 ### ECE 564 HW1 assignment
 In this first assignment, you are going to create a base data model for storing information about the students, TAs and professors in ECE 564. You need to populate your data model with at least 3 records, but can add more.  You will also provide a search function ("whoIs") to search an array of those objects by first name and last name.
 I suggest you create a new class called `DukePerson` and have it subclass `Person`.  You also need to abide by 2 protocols:
 1. BlueDevil
 2. CustomStringConvertible
 
 I also suggest you try to follow good OO practices by containing any properties and methods that deal with `DukePerson` within the class definition.
 */
/*:
 In addition to the properties required by `Person`, `CustomStringConvertible` and `BlueDevil`, you need to include the following information about each person:
 * Their degree, if applicable
 * Up to 3 of their best programming languages as an array of `String`s (like `hobbies` that is in `BlueDevil`)
 */
/*:
 I suggest you create an array of `DukePerson` objects, and you **must** have at least 3 entries in your array for me to test:
 1. Yourself
 2. Me (my info is in the slide deck)
 3. One of the TAs (also in slide deck)
 */
/*:
 Your program must contain the following:
 - You must include 4 of the following - array, dictionary, set, class, function, closure expression, enum, struct: I used a set, array, function, class, enum
 - You must include 4 different types, such as string, character, int, double, bool, float: I used string, int, bool,
 - You must include 4 different control flows, such as for/in, while, repeat/while, if/else, switch/case: I did for/in, if/else, break, switch
 - You must include 4 different operators from any of the various categories of assignment, arithmatic, comparison, nil coalescing, range, logical: I did arithmatic, assignment, nil coalescing, logical and comparison operations
 */
/*:
 Base grade is a 45 but more points can be earned by adding additional functions besides whoIs (like additional search), extensive error checking, concise code, excellent OO practices, and/or excellent, unique algorithms
 */
/*:
 Below is an example of what the string output from `whoIs' should look like:
 
 Ric Telford is from Morrisville, NC and is a Professor. He is proficient in Swift, C and C++. When not in class, Ric enjoys Biking, Hiking and Golf.
 */


enum GenderPN : String {
    case He = "He"
    case She = "She"
}

class Person {
    var firstName = "First"
    var lastName = "Last"
    var whereFrom = "Anywhere"  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
}


protocol BlueDevil {
    var hobbies : [String] { get }
    var role : DukeRole { get }
}
//: ### START OF HOMEWORK
//: Do not change anything above.
//: Put your code here:



class DukePerson: NSObject, NSCoding, BlueDevil{
    
    private let languageLimit = 3
    private let hobbyLimit = 3.0
    var hobbies: [String]
    private var languages: [String]
    var role: DukeRole
    private var major: [String]
    private var isMe: Bool
    private var degree: DukeMajor
    private var details: UIViewController?
    private var firstName: String
    private var lastName: String
    private var whereFrom: String
    private var gender: Gender
    private var image: UIImage?
    private var team = "none"
    
    //MARK: NSCoding (adapted from tutorial)
    func encode(with aCoder: NSCoder){
        //print("reeeeeeeeeeee me")
        aCoder.encode(firstName, forKey: PropertyKey.firstName)
        //print("good stuff")
        aCoder.encode(lastName, forKey: PropertyKey.lastName)
        //print("good stuff last")
        aCoder.encode(major, forKey: PropertyKey.major)
        //print("good stuff degree")
        aCoder.encode(hobbies, forKey: PropertyKey.hobbies)
        //print("good stuff hobbies")
        aCoder.encode(languages, forKey: PropertyKey.languages)
        //print("good stuff languages")
        aCoder.encode(whereFrom, forKey: PropertyKey.location)
        //print("where")
        aCoder.encode(image, forKey: PropertyKey.image)
        //print("kms")
        aCoder.encode(team, forKey: PropertyKey.team)
        //print("meme")
        aCoder.encode(details, forKey: PropertyKey.details)
        //print("details")
        aCoder.encode(role.rawValue, forKey: PropertyKey.role)
        aCoder.encode(gender.rawValue, forKey: PropertyKey.gender)
        //print("gender")
        aCoder.encode(degree.rawValue, forKey: PropertyKey.degree)
        //print("degree")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        // The name is required. If we cannot decode a name string, the initializer should fail.

        guard let firstName = aDecoder.decodeObject(forKey: PropertyKey.firstName) as? String else {
            os_log("Unable to decode the name for a First Name object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let lastName = aDecoder.decodeObject(forKey: PropertyKey.lastName) as? String else {
            os_log("Unable to decode the name for a Lat Name object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let major = aDecoder.decodeObject(forKey: PropertyKey.major) as? [String] else {
            os_log("Unable to decode the name for a Major object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let languages = aDecoder.decodeObject(forKey: PropertyKey.languages) as? [String] else {
            os_log("Unable to decode the name for a languages object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let hobbies = aDecoder.decodeObject(forKey: PropertyKey.hobbies) as? [String] else {
            os_log("Unable to decode the name for a hobbies object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let location = aDecoder.decodeObject(forKey: PropertyKey.location) as? String else {
            os_log("Unable to decode the name for a location object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        guard let image = aDecoder.decodeObject(forKey: PropertyKey.image) as? UIImage else {
            os_log("Unable to decode the name for a First Name object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let gender = aDecoder.decodeObject(forKey: PropertyKey.gender) as? String else {
            os_log("Unable to decode the name for a Lat Name object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let details = aDecoder.decodeObject(forKey: PropertyKey.details) as? UIViewController else {
            os_log("Unable to decode the name for a Major object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let team = aDecoder.decodeObject(forKey: PropertyKey.team) as? String else {
            os_log("Unable to decode the name for a languages object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let role = aDecoder.decodeObject(forKey: PropertyKey.role) as? String else {
            os_log("Unable to decode the name for a hobbies object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let degree = aDecoder.decodeObject(forKey: PropertyKey.degree) as? String else {
            os_log("Unable to decode the name for a location object.", log: OSLog.default, type: .debug)
            return nil
        }
        self.init(firstName:firstName, lastName:lastName, hobbies:hobbies, languages:languages, role:role, whereFrom: location, major:major, degree:degree, gender:gender, view:details, team:team, image:image)
    }
    
    //MARK: Archiving Paths
    
    static var DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveUrl = DocumentsDirectory.appendingPathComponent("people")
    
    init(firstName:String, lastName:String){
        self.hobbies = [String]()
        self.languages = [String]()
        self.major = [String]()
        self.role = DukeRole.Student
        self.degree = DukeMajor.BS
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        self.firstName = firstName
        self.lastName = lastName
        self.gender = Gender.Male
        self.whereFrom = "nowhere"
    }
    
    init(firstName:String, lastName:String, hobbies:[String], languages:[String],role:DukeRole, whereFrom: String, major:[String], degree:DukeMajor){
        self.hobbies = hobbies
        self.languages = languages
        self.role = role
        self.major = major
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        self.degree=degree
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = Gender.Male
    }
    
    init(firstName:String, lastName:String, hobbies:[String], languages:[String],role:DukeRole, whereFrom: String, major:[String], degree:DukeMajor, gender:Gender){
        self.hobbies = hobbies
        self.languages = languages
        self.role = role
        self.major = major
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        self.degree=degree
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
    }
    
    init(firstName:String, lastName:String, hobbies:[String], languages:[String],role:String, whereFrom: String, major:[String], degree:String, gender:String, view:UIViewController,team:String, image:UIImage){
        self.hobbies = hobbies
        self.languages = languages
        self.role = DukeRole(rawValue: role)!
        self.major = major
        self.isMe = (firstName == "Teddy" || firstName == "Theodore") && lastName == "Franceschi"
        self.degree=DukeMajor(rawValue: degree)!
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = Gender(rawValue: gender)!
        self.details = view
        self.team = team
        self.image = image
    }
    
    
    func addHobby(hobby: String){
        if(!self.hobbies.contains(hobby)){
            self.hobbies.append(hobby)
        }
    }
    
    func addLanguage(language: String){
        if(!self.languages.contains(language)){
            self.languages.append(language)
        }
    }
    
    func addMajor(major: String){
        self.major.append(major)
    }
    
    func setHobbies(hobbies: [String]){
        self.hobbies = hobbies
    }
    
    func setDetails(details: UIViewController?){
        self.details = details;
    }
    
    func setLanguages(languages: [String]){
        self.languages = languages
    }
    
    func setMajor(major: [String]){
        self.major = major
    }
    
    func setDegree(degree: String){
        self.degree = DukeMajor(rawValue: degree)!
    }
    
    func setRole(role: DukeRole){
        self.role = role
    }
    
    func setWhereFrom(whereFrom: String){
        self.whereFrom = whereFrom
    }
    
    func setImage(image: UIImage){
        self.image = image;
    }
    
    func getFirstName()->String{
        return self.firstName
    }
    
    func getLastName()->String{
        return self.lastName
    }
    
    func getLocation()->String{
        return self.whereFrom
    }
    func getImage()->UIImage{
        print("here")
        print(self.image == nil)
        return self.image!
    }
    func getHobbies()->String{
        return arrayToString(list: self.hobbies)
    }
    func getLanguages()->String{
        return arrayToString(list: self.languages)
    }
    func getGender()->String{
        return self.gender.rawValue
    }
    func getOccupation()->String{
        return self.role.rawValue
    }
    func getMajor()->String{
        return arrayToString(list: self.major)
    }
    func getDegree()->String{
        return self.degree.rawValue
    }
    func getView()->UIViewController?{
        return self.details
    }
    func getTeam()->String{
        return self.team
    }
    
    func hasAttribute(index: String)->Bool{
        if(searchListUtility(index: index, list: self.languages)){
            return true
        }
        if(searchListUtility(index: index, list: self.hobbies)){
            return true
        }
        if(self.major.contains(index)){
            return true
        }
        if(index == self.role.rawValue){
            return true
        }
        if(index == self.whereFrom){
            return true
        }
        if(index == self.lastName){
            return true
        }
        if(index == self.firstName){
            return true
        }
        if(index == "\(self.firstName) \(self.lastName)"){
            return true
        }
        if(index == self.gender.rawValue){
            return true
        }
        if(index == self.degree.rawValue){
            return true
        }
        return false
    }
    
    private func arrayToString(list: [String])->String{
        var sol = ""
        for val in list{
            sol="\(sol) \(val)"
        }
        return sol
    }
    
    private func searchListUtility(index:String, list:[String])->Bool{
        for element in list{
            if(element == index){
                return true
            }
        }
        return false
    }
    
    private func descriptionLanguages(baseResponse: String, genderPronoun: String)->String{
        var response = baseResponse
        if(self.languages.count > 0){
            let insert = self.isMe ? "I am " : "\(genderPronoun) is "
            response = "\(response) \(insert)proficient in "
            response = descriptionListUtility(limit:self.languageLimit, message: response, list:self.languages)
        }
        return response
    }
    
    private func descriptionHobbies(baseResponse: String)->String{
        var response = baseResponse
        if(self.hobbies.count>0){
            let insert = self.isMe ? "I enjoy " : "\(self.firstName) enjoys "
            response = "\(response)When not in class, \(insert)"
            response = descriptionListUtility(limit: Int(self.hobbyLimit), message: response, list: self.hobbies)
        }
        return response
    }
    
    private func descriptionListUtility(limit: Int, message: String, list:[String])->String{
        var response = message
        for (index,element) in list.enumerated(){
            if(index == 0 && index == list.count-1){
                response = "\(response)\(element). "
            }
            else if(index == 0){
                response = "\(response)\(element)"
            }
            else if(index == limit - 1 && limit != 0){
                response = "\(response) and \(element). "
                break
            }
            else if(index < list.count - 1){
                response = "\(response), \(element)"
            }
            else{
                response = "\(response) and \(element). "
            }
        }
        return response
    }
    
    override var description: String{
        let genderPronoun = self.gender == Gender.Male ? GenderPN.He.rawValue : GenderPN.She.rawValue
        let insertIntro = self.isMe ? "My name is \(self.firstName) \(self.lastName) and I am " : "\(self.firstName) \(self.lastName) is "
        //let insertEnd = self.isMe ? "am" : "is"
        var insertEnd = "am"
        switch self.isMe{
        case true: insertEnd = "am"
        case false: insertEnd = "is"
        }
        let major = self.major.first
        let end = self.role == DukeRole.Student && major != nil ? "\(self.role) studying \(major ?? "major")." : "\(self.role)."
        let baseResponse = "\(insertIntro)from \(self.whereFrom) and \(insertEnd) a \(end)"
        let languageResponse = descriptionLanguages(baseResponse: baseResponse, genderPronoun: genderPronoun)
        let finalResponse = descriptionHobbies(baseResponse: languageResponse)
        return finalResponse
    }
    
    
}

class DukePersonDB{
    var list = [DukePerson]();
    
    func addPerson(person: DukePerson){
        for (index,entry) in list.enumerated(){
            if(person.getFirstName() == entry.getFirstName() && person.getLastName() == entry.getLastName()){
                list.remove(at:index);
            }
        }
        list.append(person)
        print(list.count);
    }
    
    func search(tag: String)->[DukePerson]{
        var hitList = list
        for (index, entry) in hitList.enumerated(){
            if(!entry.hasAttribute(index: tag)){
                hitList.remove(at: index)
            }
        }
        return hitList
    }
    func getList()->[DukePerson]{
        return list
    }
    
}

class DukePersonList{
    
    private var db = [[DukePerson](),[DukePerson](),[DukePerson]()]
    private var people = [DukePerson]()
    private var labels = ["Professor","TA","Teamless Students"]
    private var teamDB = [String:[DukePerson]]()
    private var teamNum = [String:Int]()
    private var teamCount = 0
    
    init(){
        
    }
    
    func getDB()->[[DukePerson]]{
        return db
    }
    
    func getAll()->[DukePerson]{
        return people
    }

    func addPerson(person: DukePerson){
        if(person.getOccupation()==DukeRole.Professor.rawValue){
            db[0].append(person)
        }
        else if(person.getOccupation()==DukeRole.Student.rawValue){
            
            if(person.getTeam() != ""){
                if(teamDB[person.getTeam()] != nil){
                    teamDB[person.getTeam()]?.append(person)
                    db[teamNum[person.getTeam()]!].append(person)
                    //print(person)
                    //print(teamDB)
                    //print(db)
                    //print(people)
                }
                else{
                    teamCount+=1
                    labels.append(person.getTeam())
                    teamDB[person.getTeam()] = [DukePerson]()
                    teamDB[person.getTeam()]?.append(person)
                    teamNum[person.getTeam()] = teamCount+2
                    db.append(teamDB[person.getTeam()]!)
                    print(labels)
                    db[teamNum[person.getTeam()]!].append(person)
                    print(db)
                }
            }
            else{
                db[2].append(person)
            }
        }
        else if(person.getOccupation()==DukeRole.TA.rawValue){
            db[1].append(person)
        }
        people.append(person)
    }
    func removePerson(person: DukePerson){
        db[0] = db[0].filter{$0 !== person}
        db[1] = db[1].filter{$0 !== person}
        db[2] = db[2].filter{$0 !== person}
        people = people.filter{$0 !== person}
        
    }
    func replacePerson(_ person: DukePerson, _ row: Int, _ col: Int){
        print(person.getTeam())
        if(person.getOccupation()==DukeRole.Student.rawValue && person.getTeam() != ""){
            db[row].remove(at: col)
            addPerson(person: person)
        }
        else{
            self.db[row][col] = person
        }
        
        
    }
    func removePersonByCoordinates(_ row: Int, _ col: Int){
        let delete = db[row][col]
        removePerson(person: delete)
        people = people.filter{$0 !== delete}
    }
    func getLabels()->[String]{
        return labels
    }
    func getSection(_ team: String)->Int{
        if(teamNum[team] == nil){
            return 2
        }
        return teamNum[team]!
    }
}

struct PropertyKey{
    static let firstName = "first_name"
    static let lastName = "last_name"
    static let major = "major"
    static let degree = "degree"
    static let hobbies = "hobbies"
    static let languages = "languages"
    static let role = "role"
    static let location = "location"
    static let gender = "gender"
    static let details = "details"
    static let team = "team"
    static let image = "image"
}



//: ### END OF HOMEWORK
//: Uncomment the line below to test your homework.
//: The "whoIs" function should be defined as `func whoIs(_ name: String) -> String {   }`
/*
var Teddy = DukePerson(firstName: "Teddy", lastName: "Franceschi")
Teddy.setWhereFrom(whereFrom: "Las Vegas, Nevada")
Teddy.addLanguage(language: "Java")
Teddy.addLanguage(language: "Python")
Teddy.addLanguage(language: "TypeScript");
Teddy.addLanguage(language: "C")
Teddy.addLanguage(language: "C++")
Teddy.addHobby(hobby: "Hiking")
Teddy.addHobby(hobby: "Powerlifting")
Teddy.addHobby(hobby: "Running")
Teddy.addMajor(major: "ECE")
Teddy.addMajor(major: "CS")

var Robert = DukePerson(firstName: "Robert", lastName: "Steilberg")
Robert.setWhereFrom(whereFrom: "Richmond, Virginia")
Robert.addLanguage(language: "Javascript")
Robert.addLanguage(language: "Ruby")
Robert.addLanguage(language: "TypeScript");
Robert.addHobby(hobby: "Smash Bros")
Robert.addHobby(hobby: "Complaining")
Robert.addHobby(hobby: "Driving me crazy")
Robert.addMajor(major: "CS")

var Professor = DukePerson(firstName: "Ric", lastName: "Telford")
Professor.setWhereFrom(whereFrom: "Morrisville, NC")
Professor.setLanguages(languages:["swift","C","C++"])
Professor.setHobbies(hobbies: ["Biking","Hiking","Golf"])
Professor.setRole(role: DukeRole.Professor)

var TA = DukePerson(firstName: "Gilbert", lastName: "Brooks")
TA.setWhereFrom(whereFrom: "Shelby,NC")
TA.setDegree(degree:"BA")
TA.addMajor(major:"CS")
TA.addMajor(major:"AAS")
TA.setHobbies(hobbies: ["Computer Vision projects","tennis","traveling"])
TA.setLanguages(languages: ["Swift","Python","Java"])
TA.setRole(role: DukeRole.TA)




var dataBase = [DukePerson]()
dataBase.append(Teddy)
dataBase.append(Robert)
dataBase.append(Professor)
dataBase.append(TA)


for dp in dataBase{
    print(whoIs(input: "\(dp.getFirstName()) \(dp.getLastName())"))
    print("\n")
}

print(whoIs(input:"Ric Telford"))
*/



