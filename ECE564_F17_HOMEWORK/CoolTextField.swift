//
//  CoolTextField.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation
import UIKit

class CoolTextField: UITextField{
    required init(frame: CGRect,title:String) {
        super.init(frame: frame)
        backgroundColor = UIColor.lightGray
        textColor = UIColor.black
        placeholder = title
        layer.cornerRadius = frame.height/2
        self.textAlignment = NSTextAlignment.center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isFilled()->Bool{
        return !(self.text?.isEmpty)!
    }
    func setHighlightEmpty(){
        self.tintColor = UIColor.red
    }
    func setHighlightFilled(){
        self.tintColor = UIColor.white
    }
}
