//
//  Subject.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/14/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation

class Subject{
    private var observerList = [Observer]()
    private var _number = Int()
    var number : Int{
        set{
            _number = newValue
            notify()
        }
        get{
            return _number
        }
    }
    
    func attachObserver(observer: Observer){
        observerList.append(observer)
    }
    
    func removeObserver(observer: Observer){
        observerList = observerList.filter{ $0.id != observer.id}
    }
    
    private func notify(){
        for observer in observerList{
            observer.update()
        }
    }
    
}
