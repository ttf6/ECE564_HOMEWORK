//
//  ViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Ric Telford on 7/16/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit
import os.log

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    //MARK: Navigation, adapted from Apple Tutorials
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var takePhoto: UIButton!
    @IBOutlet weak var details: UIButton!
    var person: DukePerson?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
            let hobbies = genArray(list: (textFieldMap["Hobbies"]?.text)!)
            let firstName = textFieldMap["First Name"]?.text ?? "empty"
            let lastName = textFieldMap["Last Name"]?.text ?? "empty"
            let languages = genArray(list: (textFieldMap["Languages"]?.text)!)
            let role = DukeRole(rawValue: (textFieldMap["Occupation"]?.text)!)
            let gender = Gender(rawValue: (textFieldMap["Gender"]?.text)!)
            let degree = DukeMajor(rawValue: (textFieldMap["Degree"]?.text)!)
            let location = textFieldMap["Location"]?.text
            let majors = genArray(list: (textFieldMap["Major"]?.text)!)
            let team = textFieldMap["Team"]?.text ?? ""
            if(!(majors==[""]||location==""||degree?.rawValue==""||gender?.rawValue==""||role?.rawValue==""||languages==[""]||firstName==""||lastName=="")){
                print(team)
                person = DukePerson(firstName: firstName, lastName: lastName, hobbies: hobbies, languages: languages, role: role!.rawValue, whereFrom: location!, major: majors, degree: degree!.rawValue, gender: gender!.rawValue,view:TeddyViewController(),team:team,image:#imageLiteral(resourceName: "Teddy"))
                querryLabel.text="Entry Added!"
                //print(majors)
                clearForm()
            }
        
    
    }
    
    //MARK: Actions
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    //Adapted from tutorial
    @IBAction func takePhoto(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photo.contentMode = .scaleToFill
            photo.image = pickedImage
            person?.setImage(image: pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    private var labelCounter = 0;
    private var textFieldMap:[String:CoolTextField] = [:]
    private var dataBase = DukePersonDB()
    private var genderData = ["Male","Female"]
    private var degreeData = ["BS","BA","MEng","MS","PhD","MBA","MD"]
    private var roleData = ["Student","Teaching Assistant","Professor"]
    
    var genderPicker = UIPickerView()
    var rolePicker = UIPickerView()
    var degreePicker = UIPickerView()
    
    let querryLabel = UITextView(frame: CGRect(x:10, y:400, width:300, height:100))
    let labelsList:[String]=["First Name","Last Name","Location","Hobbies","Languages","Occupation","Major","Gender","Degree","Team"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let submitButton = CoolButton(frame: CGRect(x: 75, y: 515, width: 75, height: 30),title:"Submit")
        submitButton.addTarget(self, action: #selector(submitAction), for:.touchUpInside)
        let findButton = CoolButton(frame: CGRect(x: 175, y: 515, width: 75, height: 30),title:"Search")
        findButton.addTarget(self, action: #selector(findAction), for:.touchUpInside)
        querryLabel.backgroundColor = UIColor.lightGray
        querryLabel.isEditable = false
        querryLabel.adjustsFontForContentSizeCategory = true
        querryLabel.tintColor = UIColor.cyan
        genAllLabels(list:labelsList)
        
        setUpPickers()

        
        if let person = person{
            textFieldMap["First Name"]?.text = person.getFirstName()
            textFieldMap["Last Name"]?.text = person.getLastName()
            textFieldMap["Location"]?.text = person.getLocation()
            textFieldMap["Hobbies"]?.text = person.getHobbies()
            textFieldMap["Languages"]?.text = person.getLanguages()
            textFieldMap["Occupation"]?.text = person.getOccupation()
            textFieldMap["Major"]?.text = person.getMajor()
            textFieldMap["Gender"]?.text = person.getGender()
            textFieldMap["Degree"]?.text = person.getDegree()
            textFieldMap["Team"]?.text = person.getTeam()
        }
    }
    
    private func setUpPickers(){
        genderPicker.tag = 0
        genderPicker.dataSource = self
        genderPicker.delegate = self
        
        rolePicker.tag = 1
        rolePicker.dataSource = self
        rolePicker.delegate = self
        
        degreePicker.tag = 2
        degreePicker.dataSource = self
        degreePicker.delegate = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(pickerView.tag == 0){
            return genderData.count
        }
        else if(pickerView.tag == 1){
            return roleData.count
        }
        else{
            return degreeData.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            textFieldMap["Gender"]?.text = genderData[row]
        }
        else if(pickerView.tag == 1){
            textFieldMap["Occupation"]?.text = roleData[row]
        }
        else{
            textFieldMap["Degree"]?.text = degreeData[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0){
            return genderData[row]
        }
        else if(pickerView.tag == 1){
            return roleData[row]
        }
        else{
            return degreeData[row]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func submitAction(sender: UIButton!){
        let hobbies = genArray(list: (textFieldMap["Hobbies"]?.text)!)
        let firstName = textFieldMap["First Name"]?.text ?? "empty"
        let lastName = textFieldMap["Last Name"]?.text ?? "empty"
        let languages = genArray(list: (textFieldMap["Languages"]?.text)!)
        let role = DukeRole(rawValue: (textFieldMap["Occupation"]?.text)!)
        let gender = Gender(rawValue: (textFieldMap["Gender"]?.text)!)
        let degree = DukeMajor(rawValue: (textFieldMap["Degree"]?.text)!)
        let location = textFieldMap["Location"]?.text
        let majors = genArray(list: (textFieldMap["Major"]?.text)!)
        if(!(majors==[""]||location==""||degree?.rawValue==""||gender?.rawValue==""||role?.rawValue==""||languages==[""]||firstName==""||lastName=="")){
            let Teddy = DukePerson(firstName: firstName, lastName: lastName, hobbies: hobbies, languages: languages, role: role!, whereFrom: location!, major: majors, degree: degree!, gender: gender!)
            //print(Teddy)
            self.dataBase.addPerson(person: Teddy)
            print("built")
            querryLabel.text="Entry Added!"
        }
        else{
            querryLabel.text="Invalid Input!"
        }

        clearForm()
        //print(dataBase)
    }
    
    func findAction(sender: UIButton!)->String{
        var list = [String]()
        var hitList = dataBase.getList()
        //print(dataBase.getList())
        for field in textFieldMap.keys{
            let val = textFieldMap[field]?.text
            if((val) != ""){
                list.append(val!)
            }
            
        }
        //print(list)
        for person in hitList{

            for tag in list{
                if (!person.hasAttribute(index: tag)){
                    hitList = hitList.filter { $0 === person }
                }
            }
        }
        //print(hitList)
        clearForm()
        var val = ""
        for person in hitList{
            val="\(val)\n\(person.getFirstName()):\(person.description)"
        }
        querryLabel.text = val
        //print(val)
        return val
    }
    
    
    private func genLabel(text:String){
        let calcY = labelCounter*35+75
        let newLabel = CoolLabel(frame: CGRect(x: 10, y: calcY, width: 100, height: 30), title: text)
        self.view.addSubview(newLabel)
        labelCounter+=1
    }
    private func genTextField(text:String){
        let calcY = labelCounter*35+75
        let newTextField = CoolTextField(frame: CGRect(x: 110, y: calcY, width: 200, height: 30), title:text)
        if(text == "Gender"){
            newTextField.inputView = genderPicker
        }
        else if(text == "Occupation"){
            newTextField.inputView = rolePicker
        }
        else if(text == "Degree"){
            newTextField.inputView = degreePicker
        }
        self.view.addSubview(newTextField)
        textFieldMap[text]=newTextField
        
    }
    private func genAllLabels(list:[String]){
        for name in list{
            genTextField(text: name)
            genLabel(text: name)
        }
        //print(textFieldMap)
    }
    private func genArray(list: String)->[String]{
        if(list.contains(",")){
            return list.components(separatedBy: ",")
        }
        else if(list.contains(" ")){
            return list.components(separatedBy: " ")
        }
        else {
            return [list]
        }
        
    }
    
    private func clearForm(){
        for field in textFieldMap.keys{
            //print(textFieldMap[field]?.text ?? "no value");
            textFieldMap[field]?.text = ""
        }
    }
    
    func toggleFields(){
        for key in textFieldMap.keys{
            textFieldMap[key]?.isEnabled = !(textFieldMap[key]?.isEnabled)!
        }
        saveButton.title = "Save"
    }
    


}

