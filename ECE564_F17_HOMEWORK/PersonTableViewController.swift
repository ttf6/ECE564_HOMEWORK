//
//  PersonTableViewController.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/20/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import UIKit
import os.log

class PersonTableViewController: UITableViewController {

    //MARK: Properties
    var personList = DukePersonList()
    //var dataBase = [DukePerson]()
    var sections = ["Professor","TA","Teamless Students"]
    
    //MARK: Actions, adapted from Apple Tutorials
    @IBAction func unwindToPersonList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ViewController, let person = sourceViewController.person {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                print(selectedIndexPath.row)
                print(selectedIndexPath.section)
                personList.replacePerson(person, getSection(person), selectedIndexPath.row)
                sections = personList.getLabels()
            }
            else{
                personList.addPerson(person: person)
                sections = personList.getLabels()
                //print(sections)
                let sectionNum = getSection(person)
                //print(sectionNum)
                print(personList.getDB()[sectionNum].count)
                let newIndexPath = IndexPath(row: personList.getDB()[sectionNum].count-1, section: sectionNum)
                print(newIndexPath)
                print(personList.getDB())
                print(personList.getAll())
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            savePeople()
        }
    }
    
    //MARK: - Navigation, adapted from Apple Tutorials
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "AddPerson":
            os_log("Add person.", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let detailViewController = segue.destination as? ViewController else {
                fatalError("Unexpected destination")
            }
            
            guard let selectedPersonCell = sender as? PersonTableViewCell else {
                fatalError("Unexpected sender")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedPersonCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedPerson = personList.getDB()[indexPath.section][indexPath.row]
            detailViewController.person = selectedPerson
            
        default:
            fatalError("Unexpected Segue Identifier")
        }
    }
    
    private func getSection(_ person: DukePerson)->Int{
        print(person)
        if(person.getOccupation()==DukeRole.Professor.rawValue){
            return 0
        }
        else if(person.getOccupation()==DukeRole.Student.rawValue){
            if(person.getTeam() != ""){
                print(personList.getSection(person.getTeam()))
                return personList.getSection(person.getTeam())
            }
            return 2
        }
        else{
           return 1
        }
    }
    
    private func loadPeople(){
        //let teddy = DukePerson(firstName: "Teddy", lastName: "Franceschi", hobbies: ["Lifting","Hiking"], languages: ["Java","Swift"], role: DukeRole.Student.rawValue, whereFrom: "NV", major: ["CS","ECE"], degree: DukeMajor.BS.rawValue, gender: Gender.Male.rawValue, view: TeddyViewController(), team:"Gargs", image:#imageLiteral(resourceName: "Teddy"))
        //let willie = DukePerson(firstName: "Willie", lastName: "Franceschi", hobbies: ["Running","Frisbee"], languages: ["Java","C"], role: DukeRole.Student, whereFrom: "MD", major: ["BME","PreMed"], degree: DukeMajor.BS, gender: Gender.Male)
        //let robert = DukePerson(firstName: "Robert", lastName: "Steilberg", hobbies: ["Scuba","Smash Bros"], languages: ["Java","Ruby"], role: DukeRole.Student, whereFrom: "VA", major: ["CS"], degree: DukeMajor.BS, gender: Gender.Male)
        //let teacher = DukePerson(firstName: "Ric", lastName: "Telford", hobbies: ["biking","walking"], languages: ["Java","Swift"], role: DukeRole.Professor, whereFrom: "NC", major: ["CS"], degree: DukeMajor.BS, gender: Gender.Male)
        //teddy.setDetails(details: TeddyViewController())
        //personList.addPerson(person: teddy)
        //personList.addPerson(person: willie)
        //personList.addPerson(person: robert)
        //personList.addPerson(person: teacher)
        
    }
    
    private func loadDukies()->[DukePerson]?{
        return NSKeyedUnarchiver.unarchiveObject(withFile:DukePerson.ArchiveUrl.path) as? [DukePerson]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.sections = personList.getLabels()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if let savedPeople = loadDukies(){
            print("success!")
            print(savedPeople)
            for dukie in savedPeople{
                personList.addPerson(person: dukie)
            }
        }
        //loadPeople()
        else{
            loadPeople()
            print("REEEEEE y u fail me")
        }
        self.sections = personList.getLabels()
        print(self.sections)
        print(personList.getDB())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return personList.getDB()[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PersonTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PersonTableViewCell.")
        }

        // Configure the cell...
        let person = personList.getDB()[indexPath.section][indexPath.row]
        cell.nameLabel.text = "\(person.getFirstName()) \(person.getLastName())"
        cell.descriptionText.text = "\(person.description)"
        cell.descriptionText.isEditable = false
        cell.personView = person.getView()
        cell.personImage = UIImageView()
        cell.navigation = self.navigationController
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
 

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            personList.removePersonByCoordinates(indexPath.section, indexPath.row)
            savePeople()
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    private func savePeople(){
        let savedProperly = NSKeyedArchiver.archiveRootObject(personList.getAll(), toFile: DukePerson.ArchiveUrl.path)
        if(savedProperly){
            os_log("People have been added.", log: OSLog.default, type: .debug)
        }
        else{
            os_log("Saving failed", log: OSLog.default, type: .error)
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
