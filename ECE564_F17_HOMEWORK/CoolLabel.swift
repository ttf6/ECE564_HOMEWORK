//
//  CoolLabel.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/12/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation
import UIKit

class CoolLabel:UILabel{
    required init(frame: CGRect,title:String) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        textAlignment = NSTextAlignment.center
        text=title
        textColor = UIColor.black
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(newText: String){
        text = newText
    }
}
