//
//  Observer.swift
//  ECE564_F17_HOMEWORK
//
//  Created by Theodore Franceschi on 9/14/17.
//  Copyright © 2017 ece564. All rights reserved.
//

import Foundation

protocol Observer{
    var id : Int{ get }
    func update()
}
