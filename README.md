# Homework 1: Storyboard

##### Additional Features/Design Remarks:
  - Querying function (queryIndex) that allows user to query any attribute (name, profession, hobbies, etc)
  - User can store more than 3 languages and hobbies and can keep adding to these after these have been constructed (people always keep learning and find new hobbies everyday and this is reflected)
  - User can limit how many languages and hobbies are displayed in the description for each DukePerson (see languageLimit & hobbyLimit)
  - Multiple Constructors and some setters to allow the user to create a DukePerson with various amounts of information and to edit them in the future (See add and set functions)

# Homework 2: First App

##### Additional Features/Design Remarks:
- Querying function is now usable such that you can query database by inputting any field and finding a list of results.  You can still find specific person by entering first and last name as well
- If you want to change a user, input the same first and last name and just repopulate the fields on the form and submit it.  The older version will be removed
- You cannot submit unless all fields are full
- Display for results resizes font based on size of "database hit"
- Drop downs are availabe for fields with "standard responses" i.e. role or degree
- It may just be on the simulator but when using the drop down, to exit out of it you need to click another textfield

# Homework 3: List View

##### Additional Features/Design Remarks:
- Added delete functionality so you can delete any entry you add
- You cannot save over a file unless all fields are populated (can't break an existing person)
- You cannot create an empty person, all fields must be populated
- Note: The tutorial I referenced several times in my code is [here](https://developer.apple.com/library/content/referencelibrary/GettingStarted/DevelopiOSAppsSwift/ImplementEditAndDeleteBehavior.html#//apple_ref/doc/uid/TP40015214-CH9-SW)

# Homework 4: Animation

##### Additional Features/Design Remarks:
- Adds details button to each cell to allow user to see card
- You can only view a card for me currently
- Used animation for stars, multiple layers for the tent to give it a 3d look
- Shooting stars repeat every 2 seconds

# Homework 5: Persistence

##### Additional Features/Design Remarks:
- Data can be saved (including image) 
- Data can be reloaded after saving
- Cells can be deleted
- Team functionality exists in the backend but I shot myself in the foot for time so not fully integrated in front end (so don't use it when grading please)
- Photo functionality is almost setup but for same reason above, not fully integrated
